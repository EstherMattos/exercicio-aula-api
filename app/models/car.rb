class Car < ApplicationRecord
  belongs_to :manufacturer, optional: true
  has_many :rends
  has_many :sales
end
