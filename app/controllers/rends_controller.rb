class RendsController < ApplicationController
  before_action :set_rend, only: [:show, :update, :destroy]

  # GET /rends
  def index
    @rends = Rend.all
    @rends.count_price_rends()
    render json: @rends
  end

  # GET /rends/1
  def show
    render json: @rend
  end

  # POST /rends
  def create
    @rend = Rend.new(rend_params)

    if @rend.save
      render json: @rend, status: :created, location: @rend
    else
      render json: @rend.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /rends/1
  def update
    if @rend.update(rend_params)
      render json: @rend
    else
      render json: @rend.errors, status: :unprocessable_entity
    end
  end

  # DELETE /rends/1
  def destroy
    @rend.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rend
      @rend = Rend.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def rend_params
      params.require(:rend).permit(:car_id, :start, :end)
    end
end
