class AddPriceToRend < ActiveRecord::Migration[5.2]
  def change
    add_column :rends, :price, :integer
  end
end
