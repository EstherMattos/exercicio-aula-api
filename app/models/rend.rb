class Rend < ApplicationRecord
  belongs_to :car


  def count_price_rend
    if (final - start).to_i < 30
      return car.day_price * (final - start).to_i
    elsif (final - start).to_i >= 30 && (final - start).to_i < 360
      return car.month_price * ((final - start).to_i/30)
    else (final - start).to_i >=360
      return car.year_price * ((final - start).to_i/360)
    end
  end


end
